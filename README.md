### Libs
* Para navegação entre telas:
	* @react-navigation/native
		* Seguindo com as recomendações na docs do React Navigation:
			* react-native-gesture-handler
			* react-native-reanimated
			* react-native-screens
			* react-native-safe-area-context
			* @react-native-community/masked-view

* Tipos de navegação:
	* @react-navigation/stack

* Armazenamento de dados localmente:
	* @react-native-community/async-storage

* Para interação com APIs REST:
	* axios

### Observações
* Para importar o arquivo do postman no PostmanWeb, precisei converter o arquivo de v1 para v2. (App_Empresas_v2)