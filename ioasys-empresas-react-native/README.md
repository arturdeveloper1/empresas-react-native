# Apresentação

Olá! Me chamo Artur Braga Pinheiro.
Aqui você verá o resultado de algumas horas codadas por mim para o teste proposto pela Ioasys! 😃

### Utilização

No projeto foi utilizado o Expo!

* Após clonar o projeto e acessar a pasta `ioasys-empresas-react-native`, execute um dos comandos abaixo:
	* `expo start`
	* `yarn start`
	* `npm start`