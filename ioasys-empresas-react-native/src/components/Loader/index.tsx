import React from 'react'
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native'

interface LoaderProps {
  message?: string
}

const Loader: React.FC<LoaderProps> = ({ message }) => {
  return (
    <View style={[StyleSheet.absoluteFill, styles.containerWrapper]}>
      <View style={styles.container}>
        <ActivityIndicator />
        <Text style={styles.containerText}>{message || 'Aguarde...'}</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  containerWrapper: {
    zIndex: 1000,

    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.6);'
  },

  container: {
    height: 70,
    width: 200,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'black'
  },

  containerText: {
    color: '#FFF',
    fontFamily: 'Roboto_400Regular',
    fontSize: 18,
    marginLeft: 10
  }
})

export default Loader
