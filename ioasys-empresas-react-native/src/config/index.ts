interface IConfig {
  endpoint: string;
}

const config: IConfig = {
  endpoint: 'https://empresas.ioasys.com.br/api/v1/'
}

export default config;