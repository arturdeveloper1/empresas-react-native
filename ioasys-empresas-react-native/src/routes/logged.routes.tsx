import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import { EnterprisesList } from '../screens/EnterprisesList'
import { EnterpriseDetail } from '../screens/EnterpriseDetail'

const Stack = createStackNavigator()

const Routes: React.FC = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{ headerShown: false }}
        name="EnterprisesList"
        component={EnterprisesList}
      />
      <Stack.Screen
        options={{ headerShown: false }}
        name="EnterpriseDetail"
        component={EnterpriseDetail}
      />
    </Stack.Navigator>
  )
}

export default Routes
