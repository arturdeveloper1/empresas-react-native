import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import { Login } from '../screens/Login'

const Stack = createStackNavigator()

const Routes: React.FC = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{ headerShown: false }}
        name="login"
        component={Login}
      />
    </Stack.Navigator>
  )
}

export default Routes
