import React from 'react'

import { useAuth } from '../contexts/auth'
import Loader from '../components/Loader'

import InitialRoutes from './initial.routes'
import LoggedRoutes from './logged.routes'

const Routes: React.FC = () => {
  const { signed, loading } = useAuth()
  if (loading) {
    return <Loader />
  }

  return signed ? <LoggedRoutes /> : <InitialRoutes />
}

export default Routes
