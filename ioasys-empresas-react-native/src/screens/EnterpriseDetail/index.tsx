import React from 'react'
import { StyleSheet, View, Text, ScrollView } from 'react-native'
import { useRoute } from '@react-navigation/core'
import { IEnterprise } from '../../types'
import colors from '../../styles/colors'


export const EnterpriseDetail = () => {
  const route = useRoute()

  const routeParams = route.params as IEnterprise

  const {
    enterprise_name,
    description,
    enterprise_type,
  } = routeParams

  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.title}>{enterprise_name}</Text>
          <Text style={styles.subTitle}>{enterprise_type.enterprise_type_name}</Text>
        </View>
        <Text style={styles.description}>{description}</Text>
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    paddingTop: 70,
    paddingHorizontal: 10
  },
  header: {
    marginBottom: 10
  },
  title: {
    fontSize: 32,
    color: colors.heading,
    fontWeight: 'bold'
  },
  subTitle: {
    fontStyle: 'italic',
    color: colors.gray,
    fontWeight: 'bold'
  },
  description: {
    fontSize: 20,
    marginBottom: 10
  }
})