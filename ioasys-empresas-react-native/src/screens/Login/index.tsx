import React, { useEffect, useState } from 'react'
import {
  View,
  StyleSheet,
  Text,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
  Alert
} from 'react-native'

import colors from '../../styles/colors'
import { Button } from '../../components/Button'
import { useAuth } from '../../contexts/auth'
import authRepository from '../../repositories/Auth'

export function Login() {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [loading, setLoading] = useState(false)

  const { setHeaders, setInvestor } = useAuth()

  const handleSubmit = async () => {
    if (!!!email || !!!password) return Alert.alert('Preencha ambos os campos.')
    setLoading(true)
    try {
      const { data, headers } = await authRepository.signIn(email, password)
      setHeaders(headers)
      setInvestor(data.investor)
    } catch (err) {
      const errors = err.response?.data?.errors || []
      Alert.alert(errors.length > 0 ? errors[0] : 'Ocorreu algum problema ao tentar realizar o login.')
      setLoading(false)
    }
  }

  return (
    <View style={styles.container}>
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} >
          <View style={styles.content}>
            <View style={styles.form}>
              <View style={styles.header}>
                <Text style={styles.title}>Acesse sua conta</Text>
              </View>
              <TextInput
                style={styles.input}
                onChangeText={setEmail}
                value={email}
                placeholder="E-mail"
                keyboardType="email-address"
              />
              <TextInput
                style={styles.input}
                onChangeText={setPassword}
                value={password}
                placeholder="Senha"
                secureTextEntry
              />

              <View style={styles.footer}>
                <Button title="Entrar" onPress={handleSubmit} disabled={loading} />
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  content: {
    flex: 1,
    width: '100%',
  },
  form: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 54
  },
  header: {
    alignItems: 'center',
    marginBottom: 10
  },
  title: {
    fontSize: 32,
    textAlign: 'center',
    color: colors.heading,
    marginTop: 20
  },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: colors.gray,
    color: colors.heading,
    width: '100%',
    fontSize: 18,
    marginTop: 10,
    padding: 10,
    textAlign: 'center'
  },
  footer: {
    width: '100%',
    marginTop: 40,
    paddingHorizontal: 20
  }
})
