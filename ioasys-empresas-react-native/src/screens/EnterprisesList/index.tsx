import { useNavigation } from '@react-navigation/core'
import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, FlatList, TouchableOpacity, TextInput } from 'react-native'
import { Feather as Icon } from '@expo/vector-icons'

import enterpriseRepository from '../../repositories/Enterprises'
import colors from '../../styles/colors'
import { IEnterprise } from '../../types'
import useFilters from '../../hooks/useFilters'

export const EnterprisesList = () => {
  const [enterprises, setEnterprises] = useState<IEnterprise[]>([])

  const navigation = useNavigation()

  const {
    queryFilter
  } = useFilters()

  const {
    query,
    setQuery,
    filterByQuery
  } = queryFilter

  useEffect(() => {
    loadEnterprise()
  }, [])

  const loadEnterprise = async () => {
    try {
      const { data } = await enterpriseRepository.index()
      setEnterprises(data.enterprises)
    } catch (err) {
      console.error(err)
    }
  }

  const handleNavigation = (item: IEnterprise) => {
    navigation.navigate('EnterpriseDetail', item)
  }

  const renderItem = ({ item }: { item: IEnterprise }) => {
    return (
      <TouchableOpacity
        onPress={() => handleNavigation(item)}
      >
        <View style={styles.itemContainer}>
          <Text style={styles.enterpriseName}>{item.enterprise_name}</Text>
          <Text style={styles.enterpriseType}>{item.enterprise_type.enterprise_type_name}</Text>
          <Text>{item.description}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  const handleFilters = (item: IEnterprise) => {
    const querySearch = [item['enterprise_name'], item['enterprise_type']['enterprise_type_name']]
    return filterByQuery(querySearch)
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Empresas</Text>
      <View style={styles.searchView}>
        <Icon
          name="search"
          style={styles.searchIcon}
        />
        <TextInput
          value={query}
          onChangeText={setQuery}
          style={styles.searchInput}
          placeholder="Pesquise por Nome e Tipo"
        />
      </View>
      <FlatList
        data={enterprises.filter(handleFilters)}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingHorizontal: 20
  },
  content: {
    flex: 1,
    width: '100%',
  },
  title: {
    fontSize: 32,
    textAlign: 'center',
    color: colors.heading,
    paddingVertical: 20
  },
  searchView: {
    flexDirection: 'row',
    borderWidth: 2,
    borderBottomColor: colors.gray,
    width: '100%',
    marginBottom: 10,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  searchIcon: {
    fontSize: 24,
    paddingLeft: 5
  },
  searchInput: {
    width: '100%',
    paddingHorizontal: 5
  },
  itemContainer: {
    paddingVertical: 10
  },
  enterpriseName: {
    fontWeight: 'bold',
    fontSize: 15
  },
  enterpriseType: {
    fontStyle: 'italic',
    color: colors.gray
  }
})