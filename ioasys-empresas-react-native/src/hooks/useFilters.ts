import { useState } from 'react'

interface IFilters {
  query?: string
}

const useFilters = (filters: IFilters = {}) => {
  const [query, setQuery] = useState(filters.query || '')


  const filterByQuery = (attributesToSearch: any[]) => {
    if (!query) {
      return true
    }
    const result = attributesToSearch.map(attribute => attribute?.toString().toLowerCase().includes(query.toLowerCase()))
    return result.some(res => res)
  }

  return {
    queryFilter: {
      query,
      setQuery,
      filterByQuery
    },
  }
}

export default useFilters