import React, { createContext, useState, useContext } from 'react'

import { IInvestor } from '../types'
import { api } from '../services/api'

interface AuthConextData {
  signed: boolean;
  investor: IInvestor | null;
  loading: boolean;
  setHeaders(headers: any): void;
  setInvestor(investor: IInvestor): void;
}

const AuthConext = createContext<AuthConextData>({} as AuthConextData)

export const AuthProvider: React.FC = ({ children }) => {
  const [investor, setInvestor] = useState<IInvestor | null>(null)
  const [loading, setLoading] = useState(false)

  const setHeaders = (headers: any) => {
    api.defaults.headers['access-token'] = headers['access-token']
    api.defaults.headers['client'] = headers['client']
    api.defaults.headers['uid'] = headers['uid']
  }

  return (
    <AuthConext.Provider
      value={{
        signed: !!investor,
        investor,
        loading,
        setHeaders,
        setInvestor
      }}
    >
      {children}
    </AuthConext.Provider>
  )
}

export function useAuth(): AuthConextData {
  const context = useContext(AuthConext)

  return context
}