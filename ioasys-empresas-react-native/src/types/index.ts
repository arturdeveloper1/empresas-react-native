import { IEnterprise } from './Enterprises'
import { IInvestor } from './Investor'

export {
  IEnterprise,
  IInvestor
}