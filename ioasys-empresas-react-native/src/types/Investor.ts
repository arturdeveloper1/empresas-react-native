import { IEnterprise } from './Enterprises'

export interface IInvestor {
  id: number;
  investor_name: string;
  email: string;
  city: string;
  country: string;
  balance: number;
  photo: string | null;
  portfolio: {
    enterprises_number: 0;
    enterprises: IEnterprise[]
  };
  portfolio_value: number;
  first_access: boolean;
  super_angel: boolean
}