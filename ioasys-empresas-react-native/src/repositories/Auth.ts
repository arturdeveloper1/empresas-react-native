import { api } from '../services/api'

const signIn = (email: string, password: string ) => {
  return api.post('users/auth/sign_in', {
    email,
    password
  })
}

export default {
  signIn
}