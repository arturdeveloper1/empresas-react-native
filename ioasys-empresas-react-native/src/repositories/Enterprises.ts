import { api } from "../services/api";

const index = () => {
  return api.get('enterprises')
}

const show = (id: number) => {
  return api.get(`enterprises/${id}`)
}

export default {
  index,
  show
}