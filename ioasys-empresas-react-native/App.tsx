import React from 'react'
import { StatusBar } from 'expo-status-bar'
import { Platform, SafeAreaView, StyleSheet } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { AuthProvider } from './src/contexts/auth'

import Routes from './src/routes'

export default function App() {

  return (
    <NavigationContainer>
      <SafeAreaView style={styles.container}>
        <AuthProvider>
          <StatusBar style="auto" />
          <Routes />
        </AuthProvider>
      </SafeAreaView>
    </NavigationContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Platform.OS === 'android' ? 25 : 0
  }
})
